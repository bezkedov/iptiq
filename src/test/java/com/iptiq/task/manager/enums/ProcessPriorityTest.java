package com.iptiq.task.manager.enums;

import static org.assertj.core.api.Assertions.assertThat;

import com.iptiq.task.manager.process.ProcessPriority;
import java.util.List;
import org.junit.jupiter.api.Test;

class ProcessPriorityTest {

    @Test
    void getLowerOrEqualHigh() {
        // Arrange
        // Act
        List<ProcessPriority> result = ProcessPriority.getLowerPriorities(ProcessPriority.HIGH);

        // Assert
        assertThat(result).hasSize(2);
        assertThat(result.get(0)).isEqualTo(ProcessPriority.LOW);
        assertThat(result.get(1)).isEqualTo(ProcessPriority.MEDIUM);
    }

    @Test
    void getLowerOrEqualMedium() {
        // Arrange
        // Act
        List<ProcessPriority> result = ProcessPriority.getLowerPriorities(ProcessPriority.MEDIUM);

        // Assert
        assertThat(result).hasSize(1);
        assertThat(result.get(0)).isEqualTo(ProcessPriority.LOW);
    }

    @Test
    void getLowerOrEqualLow() {
        // Arrange
        // Act
        List<ProcessPriority> result = ProcessPriority.getLowerPriorities(ProcessPriority.LOW);

        // Assert
        assertThat(result).hasSize(0);
    }
}