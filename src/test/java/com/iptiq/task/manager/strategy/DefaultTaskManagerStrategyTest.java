package com.iptiq.task.manager.strategy;

import static org.assertj.core.api.Assertions.assertThat;

import com.iptiq.task.manager.process.Process;
import com.iptiq.task.manager.process.ProcessPriority;
import com.iptiq.task.manager.strategy.exception.MaxCapacityReachedException;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class DefaultTaskManagerStrategyTest {

    private DefaultTaskManagerStrategy uut = new DefaultTaskManagerStrategy(10);

    @Test
    void successfullyAdd() throws MaxCapacityReachedException {
        // Arrange
        var testPriority = ProcessPriority.LOW;
        var process = new AddProcessCommand(testPriority);

        // Act
        Integer pid = uut.add(process);

        // Assert
        assertThat(uut.list()).hasSize(1);
        assertThat(uut.currentCapacity).isEqualTo(1);
        Process resultProcess = uut.list().get(0);
        assertThat(resultProcess.getPid()).isEqualTo(pid);
        assertThat(resultProcess.getPriority()).isEqualTo(testPriority);
        assertThat(resultProcess.getTimestamp()).isNotNull();
    }

    @Test
    void successfullyAddThrowMaxCapacityReachedException() {
        // Arrange
        uut = new DefaultTaskManagerStrategy(1);
        uut.add(new AddProcessCommand(ProcessPriority.LOW));

        // Act
        // Assert
        Assertions.assertThrows(MaxCapacityReachedException.class,
            () -> uut.add(new AddProcessCommand(ProcessPriority.LOW)));
    }

    @Test
    void addThrowIllegalArgumentException() {
        // Arrange
        // Act
        // Assert
        Assertions.assertThrows(IllegalArgumentException.class,
            () -> uut = new DefaultTaskManagerStrategy(0));
    }

    @Test
    void successfullyList() throws MaxCapacityReachedException {
        // Arrange
        uut.add(new AddProcessCommand(ProcessPriority.LOW));
        uut.add(new AddProcessCommand(ProcessPriority.MEDIUM));
        uut.add(new AddProcessCommand(ProcessPriority.HIGH));

        // Act
        // Assert
        assertThat(uut.list()).hasSize(3);
        assertThat(uut.currentCapacity).isEqualTo(3);
    }

    @Test
    void kill() throws MaxCapacityReachedException {
        // Arrange
        Integer pidToKill = uut.add(new AddProcessCommand(ProcessPriority.LOW));
        var expectedPriority = ProcessPriority.MEDIUM;
        uut.add(new AddProcessCommand(expectedPriority));

        // Act
        assertThat(uut.currentCapacity).isEqualTo(2);
        boolean isKilled = uut.kill(pidToKill);

        // Assert
        assertThat(isKilled).isTrue();
        assertThat(uut.list()).hasSize(1);
        assertThat(uut.currentCapacity).isEqualTo(1);
        Process resultProcess = uut.list().get(0);
        assertThat(resultProcess.getPid()).isNotEqualTo(pidToKill);
        assertThat(resultProcess.getPriority()).isEqualTo(expectedPriority);
        assertThat(resultProcess.getTimestamp()).isNotNull();

    }

    @Test
    void killGroup() throws MaxCapacityReachedException {
        // Arrange
        var expectedPid1 = uut.add(new AddProcessCommand(ProcessPriority.LOW));
        var expectedPid2 = uut.add(new AddProcessCommand(ProcessPriority.LOW));
        uut.add(new AddProcessCommand(ProcessPriority.MEDIUM));
        uut.add(new AddProcessCommand(ProcessPriority.HIGH));
        assertThat(uut.list()).hasSize(4);

        // Act
        List<Integer> killedPids = uut.killGroup(ProcessPriority.LOW);

        // Assert
        assertThat(killedPids).containsExactlyInAnyOrder(expectedPid1, expectedPid2);
        assertThat(uut.currentCapacity).isEqualTo(2);
        assertThat(uut.list().stream().map(Process::getPriority).collect(Collectors.toList()))
            .containsExactlyInAnyOrder(ProcessPriority.MEDIUM, ProcessPriority.HIGH);
    }

    @Test
    void killGroupZeroProcesses() {
        // Arrange
        uut.add(new AddProcessCommand(ProcessPriority.LOW));
        uut.add(new AddProcessCommand(ProcessPriority.LOW));
        uut.add(new AddProcessCommand(ProcessPriority.MEDIUM));
        assertThat(uut.list()).hasSize(3);

        // Act
        List<Integer> killedPids = uut.killGroup(ProcessPriority.HIGH);

        // Assert
        assertThat(killedPids).isEmpty();
        assertThat(uut.currentCapacity).isEqualTo(3);
        assertThat(uut.list().stream().map(Process::getPriority).collect(Collectors.toSet()))
            .containsExactlyInAnyOrder(ProcessPriority.MEDIUM, ProcessPriority.LOW);
    }


    @Test
    void killAll() throws MaxCapacityReachedException {
        // Arrange
        Integer lowPid = uut.add(new AddProcessCommand(ProcessPriority.LOW));
        Integer mediumPid = uut.add(new AddProcessCommand(ProcessPriority.MEDIUM));
        Integer highPid = uut.add(new AddProcessCommand(ProcessPriority.HIGH));
        assertThat(uut.list()).hasSize(3);

        // Act
        List<Integer> killedPids = uut.killAll();

        // Assert
        assertThat(uut.currentCapacity).isEqualTo(0);
        assertThat(killedPids).containsExactlyInAnyOrder(lowPid, mediumPid, highPid);

    }
}