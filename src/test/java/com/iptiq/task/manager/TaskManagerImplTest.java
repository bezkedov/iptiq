package com.iptiq.task.manager;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.iptiq.task.manager.process.Process;
import com.iptiq.task.manager.process.ProcessPriority;
import com.iptiq.task.manager.strategy.AddProcessCommand;
import com.iptiq.task.manager.strategy.TaskManagerStrategy;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class TaskManagerImplTest {

    @Mock
    private TaskManagerStrategy strategy;

    @InjectMocks
    private TaskManagerImpl uut;

    @BeforeEach
    public void setup() {
        lenient().when(strategy.list()).thenReturn(List.of(
            new Process(10, ProcessPriority.LOW, System.currentTimeMillis()),
            new Process(2, ProcessPriority.LOW, System.currentTimeMillis()),
            new Process(3, ProcessPriority.MEDIUM, System.currentTimeMillis()),
            new Process(4, ProcessPriority.MEDIUM, System.currentTimeMillis()),
            new Process(7, ProcessPriority.LOW, System.currentTimeMillis()),
            new Process(11, ProcessPriority.HIGH, System.currentTimeMillis()),
            new Process(12, ProcessPriority.HIGH, System.currentTimeMillis()),
            new Process(1, ProcessPriority.MEDIUM, System.currentTimeMillis())
        ));
    }

    @Test
    void add() {
        // Arrange
        var addProcessCommand = new AddProcessCommand(ProcessPriority.LOW);

        // Act
        uut.add(addProcessCommand);

        // Assert
        verify(strategy, times(1)).add(addProcessCommand);
    }

    @Test
    void listDefaultSortingByTime() {
        // Arrange
        // Act
        List<Process> list = uut.list();

        // Assert
        assertThat(list).hasSize(8);
        assertThat(list.get(0).getPid()).isEqualTo(10);
        assertThat(list.get(1).getPid()).isEqualTo(2);
        assertThat(list.get(2).getPid()).isEqualTo(3);
        assertThat(list.get(3).getPid()).isEqualTo(4);
        assertThat(list.get(4).getPid()).isEqualTo(7);
        assertThat(list.get(5).getPid()).isEqualTo(11);
        assertThat(list.get(6).getPid()).isEqualTo(12);
        assertThat(list.get(7).getPid()).isEqualTo(1);
    }

    @Test
    void listSortingByTime() {
        // Arrange
        // Act
        List<Process> list = uut.list(ComparatorFactory.createByTimestampComparator());

        // Assert
        assertThat(list).hasSize(8);
        assertThat(list.get(0).getPid()).isEqualTo(10);
        assertThat(list.get(1).getPid()).isEqualTo(2);
        assertThat(list.get(2).getPid()).isEqualTo(3);
        assertThat(list.get(3).getPid()).isEqualTo(4);
        assertThat(list.get(4).getPid()).isEqualTo(7);
        assertThat(list.get(5).getPid()).isEqualTo(11);
        assertThat(list.get(6).getPid()).isEqualTo(12);
        assertThat(list.get(7).getPid()).isEqualTo(1);
    }

    @Test
    void listSortedByPid() {
        // Arrange
        // Act
        List<Process> list = uut.list(ComparatorFactory.createByPidComparator());

        // Assert
        assertThat(list).hasSize(8);
        assertThat(list.get(0).getPid()).isEqualTo(1);
        assertThat(list.get(1).getPid()).isEqualTo(2);
        assertThat(list.get(2).getPid()).isEqualTo(3);
        assertThat(list.get(3).getPid()).isEqualTo(4);
        assertThat(list.get(4).getPid()).isEqualTo(7);
        assertThat(list.get(5).getPid()).isEqualTo(10);
        assertThat(list.get(6).getPid()).isEqualTo(11);
        assertThat(list.get(7).getPid()).isEqualTo(12);

    }

    @Test
    void listSortedByPriority() {
        // Arrange
        // Act
        List<Process> list = uut.list(ComparatorFactory.createByPriorityComparator());

        // Assert
        assertThat(list).hasSize(8);
        assertThat(list.get(0).getPriority()).isEqualTo(ProcessPriority.LOW);
        assertThat(list.get(1).getPriority()).isEqualTo(ProcessPriority.LOW);
        assertThat(list.get(2).getPriority()).isEqualTo(ProcessPriority.LOW);
        assertThat(list.get(3).getPriority()).isEqualTo(ProcessPriority.MEDIUM);
        assertThat(list.get(4).getPriority()).isEqualTo(ProcessPriority.MEDIUM);
        assertThat(list.get(5).getPriority()).isEqualTo(ProcessPriority.MEDIUM);
        assertThat(list.get(6).getPriority()).isEqualTo(ProcessPriority.HIGH);
        assertThat(list.get(7).getPriority()).isEqualTo(ProcessPriority.HIGH);

    }

    @Test
    void kill() {
        // Arrange
        // Act
        uut.kill(1);

        // Assert
        verify(strategy, times(1)).kill(eq(1));
    }

    @Test
    void killGroup() {
        // Arrange
        // Act
        uut.killGroup(ProcessPriority.MEDIUM);

        // Assert
        verify(strategy, times(1)).killGroup(eq(ProcessPriority.MEDIUM));

    }

    @Test
    void killAll() {
        // Arrange
        // Act
        uut.killAll();

        // Assert
        verify(strategy, times(1)).killAll();

    }
}