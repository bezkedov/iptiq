package com.iptiq.task.manager.process;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;

@Getter
public enum ProcessPriority {
    LOW(0), MEDIUM(1), HIGH(2);

    private final int priority;

    ProcessPriority(int priority) {
        this.priority = priority;
    }

    public static List<ProcessPriority> getLowerPriorities(ProcessPriority inputPriority) {
        return Arrays.stream(values())
            .filter(priority -> priority.getPriority() < inputPriority.priority)
            .sorted(Comparator.comparingInt(ProcessPriority::getPriority))
            .collect(Collectors.toList());
    }
}
