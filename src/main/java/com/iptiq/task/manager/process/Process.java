package com.iptiq.task.manager.process;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@RequiredArgsConstructor
@Setter
@Getter
@EqualsAndHashCode
@ToString
public class Process {

    @EqualsAndHashCode.Include
    private final Integer pid;
    private final ProcessPriority priority;
    private final Long timestamp;

    public void kill() {
        System.out.println("Process with ID: " + pid + " has been killed");
    }
}
