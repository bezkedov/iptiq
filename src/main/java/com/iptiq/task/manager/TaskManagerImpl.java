package com.iptiq.task.manager;

import com.iptiq.task.manager.process.Process;
import com.iptiq.task.manager.process.ProcessPriority;
import com.iptiq.task.manager.strategy.AddProcessCommand;
import com.iptiq.task.manager.strategy.TaskManagerStrategy;
import com.iptiq.task.manager.strategy.exception.MaxCapacityReachedException;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TaskManagerImpl implements TaskManager {

    protected TaskManagerStrategy processStrategy;

    TaskManagerImpl(TaskManagerStrategy processStrategy) {
        this.processStrategy = processStrategy;
    }

    @Override
    public Integer add(AddProcessCommand process) throws MaxCapacityReachedException {
        return processStrategy.add(process);
    }

    @Override
    public List<Process> list() {
        return list(Comparator.comparingLong(Process::getTimestamp));
    }

    @Override
    public List<Process> list(Comparator<Process> comparator) {
        return processStrategy.list().stream()
            .sorted(comparator)
            .collect(Collectors.toList());
    }

    @Override
    public boolean kill(Integer pid) {
        return processStrategy.kill(pid);
    }

    @Override
    public List<Integer> killGroup(ProcessPriority priority) {
        return processStrategy.killGroup(priority);
    }

    @Override
    public List<Integer> killAll() {
        return processStrategy.killAll();
    }
}
