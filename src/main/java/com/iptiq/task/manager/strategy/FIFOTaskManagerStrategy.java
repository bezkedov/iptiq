package com.iptiq.task.manager.strategy;

import com.iptiq.task.manager.process.Process;
import com.iptiq.task.manager.process.ProcessPriority;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * FIFOTaskManagerStrategy accepts all new processes through the add() method, killing and removing
 * from the TM list the oldest one (First-In, First-Out) when the max size is reached
 */
public class FIFOTaskManagerStrategy extends AbstractTaskManagerStrategy {

    private final Queue<Process> processes = new LinkedList<>();

    public FIFOTaskManagerStrategy(Integer maxCapacity) {
        super(maxCapacity);
    }

    @Override
    public synchronized Integer add(AddProcessCommand addProcessCommand) {
        if (currentCapacity >= maxCapacity) {
            var process = processes.poll();
            process.kill();
            currentCapacity--;
        }

        lastPid++;
        processes.add(
            new Process(
                lastPid,
                addProcessCommand.getPriority(),
                System.currentTimeMillis()));

        currentCapacity++;
        return lastPid;
    }

    @Override
    public synchronized List<Process> list() {
        return new ArrayList<>(processes);
    }

    @Override
    public synchronized boolean kill(Integer pid) {
        var isKilled = processes.stream()
            .filter(process -> process.getPid().equals(pid))
            .findFirst()
            .map(processes::remove)
            .orElse(false);

        if (isKilled) {
            currentCapacity--;
            return true;
        }
        return false;
    }

    @Override
    public synchronized List<Integer> killGroup(ProcessPriority priority) {
        List<Integer> killedPids = new ArrayList<>();

        for (var iterator = processes.iterator(); iterator.hasNext(); ) {
            var process = iterator.next();

            if (!process.getPriority().equals(priority)) {
                continue;
            }

            process.kill();
            killedPids.add(process.getPid());
            currentCapacity--;
            iterator.remove();
        }

        return killedPids;
    }

    @Override
    public synchronized List<Integer> killAll() {
        List<Integer> killedPids = new ArrayList<>();

        for (Process process : processes) {
            process.kill();
            killedPids.add(process.getPid());
            currentCapacity--;
        }

        processes.clear();

        return killedPids;
    }
}
