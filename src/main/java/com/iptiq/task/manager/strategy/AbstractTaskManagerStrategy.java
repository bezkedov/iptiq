package com.iptiq.task.manager.strategy;

public abstract class AbstractTaskManagerStrategy implements TaskManagerStrategy {

    Integer lastPid = 0;
    Integer currentCapacity = 0;
    final Integer maxCapacity;

    public AbstractTaskManagerStrategy(Integer maxCapacity) {
        if (maxCapacity < 1) {
            throw new IllegalArgumentException();
        }

        this.maxCapacity = maxCapacity;
    }
}
