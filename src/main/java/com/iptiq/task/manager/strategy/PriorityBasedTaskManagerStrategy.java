package com.iptiq.task.manager.strategy;

import com.iptiq.task.manager.process.Process;
import com.iptiq.task.manager.process.ProcessPriority;
import com.iptiq.task.manager.strategy.exception.MaxCapacityReachedException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.stream.Collectors;

/**
 * PriorityBasedTaskManagerStrategy. If the new process passed in the add() call has a higher
 * priority compared to any of the existing one, we remove the lowest priority that is the oldest,
 * otherwise we skip it by throwing {@link MaxCapacityReachedException}
 */
public class PriorityBasedTaskManagerStrategy extends AbstractTaskManagerStrategy {

    private final Map<ProcessPriority, Queue<Process>> processes = new EnumMap<>(
        ProcessPriority.class);

    public PriorityBasedTaskManagerStrategy(Integer maxCapacity) {
        super(maxCapacity);
        for (ProcessPriority priority : ProcessPriority.values()) {
            processes.put(priority, new LinkedList<>());
        }
    }

    @Override
    public synchronized Integer add(AddProcessCommand addProcessCommand) {
        if (currentCapacity >= maxCapacity) {
            List<ProcessPriority> lowerPriorities = ProcessPriority.getLowerPriorities(
                addProcessCommand.getPriority());
            boolean wasNotKilled = true;
            for (ProcessPriority priority : lowerPriorities) {
                Queue<Process> processesByPriority = processes.get(priority);
                if (processesByPriority.size() <= 0) {
                    continue;
                }

                var process = processesByPriority.poll();
                process.kill();
                currentCapacity--;
                wasNotKilled = false;
                break;
            }

            if (wasNotKilled) {
                throw new MaxCapacityReachedException();
            }
        }

        lastPid++;
        processes.get(addProcessCommand.getPriority()).add(
            new Process(
                lastPid,
                addProcessCommand.getPriority(),
                System.currentTimeMillis()));

        currentCapacity++;
        return lastPid;
    }

    @Override
    public synchronized List<Process> list() {
        return processes.values().stream()
            .flatMap(Collection::stream)
            .collect(Collectors.toList());
    }

    @Override
    public synchronized boolean kill(Integer pid) {
        boolean isKilled;
        for (Queue<Process> queue : processes.values()) {
            isKilled = queue.stream()
                .filter(process -> process.getPid().equals(pid))
                .findFirst()
                .map(queue::remove)
                .orElse(false);

            if (isKilled) {
                currentCapacity--;
                return true;
            }
        }

        return false;
    }

    @Override
    public synchronized List<Integer> killGroup(ProcessPriority priority) {
        List<Integer> killedPids = new ArrayList<>();

        for (var iterator = processes.get(priority).iterator(); iterator.hasNext(); ) {
            var process = iterator.next();

            process.kill();
            killedPids.add(process.getPid());
            currentCapacity--;
            iterator.remove();
        }

        return killedPids;
    }

    @Override
    public synchronized List<Integer> killAll() {
        List<Integer> killedPids = new ArrayList<>();

        for (Queue<Process> queue : processes.values()) {
            for (Process process : queue) {
                process.kill();
                killedPids.add(process.getPid());
                currentCapacity--;
            }
            queue.clear();
        }

        return killedPids;
    }
}
