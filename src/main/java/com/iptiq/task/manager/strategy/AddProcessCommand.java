package com.iptiq.task.manager.strategy;


import com.iptiq.task.manager.process.ProcessPriority;
import lombok.Data;

@Data
public class AddProcessCommand {

    private final ProcessPriority priority;
}
