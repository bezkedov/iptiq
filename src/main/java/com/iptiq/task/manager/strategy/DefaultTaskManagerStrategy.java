package com.iptiq.task.manager.strategy;

import com.iptiq.task.manager.process.Process;
import com.iptiq.task.manager.process.ProcessPriority;
import com.iptiq.task.manager.strategy.exception.MaxCapacityReachedException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * DefaultTaskManagerStrategy is that it can accept new processes till when there the max capacity
 * isn't reached, otherwise we won’t accept any new process and {@link MaxCapacityReachedException}
 * is thrown.
 */
public class DefaultTaskManagerStrategy extends AbstractTaskManagerStrategy {

    private final Map<Integer, Process> pidToProcess = new HashMap<>();

    public DefaultTaskManagerStrategy(Integer maxCapacity) {
        super(maxCapacity);
    }

    @Override
    public synchronized Integer add(AddProcessCommand addProcessCommand) {
        if (currentCapacity >= maxCapacity) {
            throw new MaxCapacityReachedException();
        }

        lastPid++;
        pidToProcess.put(lastPid,
            new Process(
                lastPid,
                addProcessCommand.getPriority(),
                System.currentTimeMillis()));

        currentCapacity++;
        return lastPid;
    }

    @Override
    public synchronized List<Process> list() {
        return new ArrayList<>(pidToProcess.values());
    }

    @Override
    public synchronized boolean kill(Integer pid) {
        var process = pidToProcess.remove(pid);
        if (process != null) {
            process.kill();
            currentCapacity--;
            return true;
        }
        return false;
    }

    @Override
    public synchronized List<Integer> killGroup(ProcessPriority priority) {
        List<Integer> killedPids = new ArrayList<>();

        for (var iterator = pidToProcess.entrySet().iterator(); iterator.hasNext(); ) {
            var process = iterator.next().getValue();

            if (!process.getPriority().equals(priority)) {
                continue;
            }

            process.kill();
            killedPids.add(process.getPid());
            currentCapacity--;
            iterator.remove();
        }

        return killedPids;
    }

    @Override
    public synchronized List<Integer> killAll() {
        List<Integer> killedPids = new ArrayList<>();
        for (Process process : pidToProcess.values()) {
            process.kill();
            killedPids.add(process.getPid());
            currentCapacity--;
        }

        pidToProcess.clear();

        return killedPids;
    }
}
