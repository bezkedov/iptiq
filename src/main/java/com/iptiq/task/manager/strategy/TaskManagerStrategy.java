package com.iptiq.task.manager.strategy;

import com.iptiq.task.manager.process.Process;
import com.iptiq.task.manager.process.ProcessPriority;
import java.util.List;

public interface TaskManagerStrategy {

    Integer add(AddProcessCommand process);

    List<Process> list();

    boolean kill(Integer pid);

    List<Integer> killGroup(ProcessPriority priority);

    List<Integer> killAll();
}
