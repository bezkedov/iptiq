package com.iptiq.task.manager;

import com.iptiq.task.manager.process.Process;
import java.util.Comparator;

public class ComparatorFactory {

    public static Comparator<Process> createByPidComparator() {
        return Comparator.comparingInt(Process::getPid);
    }

    public static Comparator<Process> createByTimestampComparator() {
        return Comparator.comparingLong(Process::getTimestamp);
    }

    public static Comparator<Process> createByPriorityComparator() {
        return Comparator.comparing(process -> process.getPriority().getPriority());
    }
}
