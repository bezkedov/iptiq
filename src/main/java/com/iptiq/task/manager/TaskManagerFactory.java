package com.iptiq.task.manager;

import com.iptiq.task.manager.strategy.DefaultTaskManagerStrategy;
import com.iptiq.task.manager.strategy.FIFOTaskManagerStrategy;
import com.iptiq.task.manager.strategy.PriorityBasedTaskManagerStrategy;

public class TaskManagerFactory {

    public static TaskManager createDefaultTaskManager(Integer maxCapacity) {
        return new TaskManagerImpl(new DefaultTaskManagerStrategy(maxCapacity));
    }

    public static TaskManager createFIFOTaskManager(Integer maxCapacity) {
        return new TaskManagerImpl(new FIFOTaskManagerStrategy(maxCapacity));
    }

    public static TaskManager createPriorityBasedTaskManager(Integer maxCapacity) {
        return new TaskManagerImpl(new PriorityBasedTaskManagerStrategy(maxCapacity));
    }
}
