package com.iptiq.task.manager;

import com.iptiq.task.manager.process.Process;
import com.iptiq.task.manager.process.ProcessPriority;
import com.iptiq.task.manager.strategy.AddProcessCommand;
import com.iptiq.task.manager.strategy.exception.MaxCapacityReachedException;
import java.util.Comparator;
import java.util.List;

/**
 * With Task Manager we refer to a software component that is designed for handling multiple
 * processes inside an operating system
 */
public interface TaskManager {

    Integer add(AddProcessCommand process) throws MaxCapacityReachedException;

    List<Process> list();

    List<Process> list(Comparator<Process> comparator);

    boolean kill(Integer pid);

    List<Integer> killGroup(ProcessPriority priority);

    List<Integer> killAll();

}
