# Task Manager

Task Manager library provides classes are designed for emulating of handling multiple 
processes inside an operating system.

Each process is identified by 2 fields, a unique unmodifiable identifier (PID), and a priority (low, medium, high).
The process is immutable, it is generated with a priority and will die with this priority – each process
has a kill() method that will destroy it.

Task Manager exposes the following functionality:

1. Add a process
2. List running processes
3. Kill by PID
4. KillGroup by priority
5. KillAll

There are tree types of Task Manager with different behaviours:

1. Default Task manager - it accepts a new processes till when there is capacity inside the Task Manager, otherwise it won’t accept
   any new process.
2. FIFO Task Manager - it kills and removes from the list ttakshe oldest one (First-In, First-Out) when the max size is reached.
3. Priority based - when the max size is reached, should result into an evaluation: if the new
   process passed in the add() call has a higher priority compared to any of the existing one, TM removes the
   lowest priority that is the oldest, otherwise TM skips it.

All Task manager can be created by a factory class com.iptiq.task.manager.TaskManagerFactory.

Each Task manager allows to get a list running process sorted by time of adding to TM or using custom comparator.
There is a factory class com.iptiq.task.manager.ComparatorFactory which allows creating tree predefined comparators: by PID, by time, by priority.